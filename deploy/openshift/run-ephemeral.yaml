apiVersion: v1
kind: Template
labels:
  app: nextcloudlookupserver
  template: nextcloudlookupserver-ephemeral
metadata:
  annotations:
    description: NextCloud Lookup Server - ephemeral
    iconClass: icon-php
    openshift.io/display-name: NextCloud Lookup Server
    tags: nextcloudlookupserver
  name: nextcloudlookupserver-ephemeral
objects:
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: mysql-${FRONTNAME}
    name: mysql-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: mysql-${FRONTNAME}
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: mysql-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - env:
          - name: MYSQL_DATABASE
            valueFrom:
              secretKeyRef:
                key: database-name
                name: nextcloudlookupserver-${FRONTNAME}
          - name: MYSQL_PASSWORD
            valueFrom:
              secretKeyRef:
                key: database-password
                name: nextcloudlookupserver-${FRONTNAME}
          - name: MYSQL_ROOT_PASSWORD
            valueFrom:
              secretKeyRef:
                key: database-admin-password
                name: nextcloudlookupserver-${FRONTNAME}
          - name: MYSQL_USER
            valueFrom:
              secretKeyRef:
                key: database-user
                name: nextcloudlookupserver-${FRONTNAME}
          imagePullPolicy: IfNotPresent
          livenessProbe:
            initialDelaySeconds: 30
            tcpSocket:
              port: 3306
            timeoutSeconds: 3
          name: mariadb
          ports:
          - containerPort: 3306
            protocol: TCP
          readinessProbe:
            exec:
              command:
              - /bin/sh
              - '-i'
              - '-c'
              - MYSQL_PWD="$MYSQL_PASSWORD" mysql -h 127.0.0.1 -u $MYSQL_USER -D $MYSQL_DATABASE -e 'SELECT 1'
            failureThreshold: 6
            initialDelaySeconds: 5
            timeoutSeconds: 3
          resources:
            limits:
              cpu: "${NEXTCLOUDLOOKUPSERVER_MYSQL_CPU_LIMIT}"
              memory: "${NEXTCLOUDLOOKUPSERVER_MYSQL_MEMORY_LIMIT}"
          volumeMounts:
          - name: db
            mountPath: /var/lib/mysql/data
        volumes:
        - emptyDir: {}
          name: db
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - mariadb
        from:
          kind: ImageStreamTag
          name: ${MYSQL_IMAGESTREAM_TAG}
          namespace: ${MYSQL_NAMESPACE}
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: mysql-${FRONTNAME}
  spec:
    ports:
    - name: mysql
      port: 3306
    selector:
      name: mysql-${FRONTNAME}
    type: ClusterIP
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: nextcloudlookupserver-${FRONTNAME}
    name: nextcloudlookupserver-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: nextcloudlookupserver-${FRONTNAME}
    strategy:
      type: Rolling
    template:
      metadata:
        labels:
          name: nextcloudlookupserver-${FRONTNAME}
      spec:
        containers:
        - env:
          - name: APACHE_DOMAIN
            value: cloud-lookup.${ROOT_DOMAIN}
          - name: APACHE_HTTP_PORT
            value: "8080"
          - name: LOOKUP_GLOBAL_SCALE
            value: "true"
          - name: LOOKUP_JWT_KEY
            valueFrom:
              secretKeyRef:
                key: jwt-key
                name: nextcloudlookupserver-${FRONTNAME}
          - name: MYSQL_DATABASE
            valueFrom:
              secretKeyRef:
                key: database-name
                name: nextcloudlookupserver-${FRONTNAME}
          - name: MYSQL_HOST
            value: mysql-${FRONTNAME}
          - name: MYSQL_PASS
            valueFrom:
              secretKeyRef:
                key: database-password
                name: nextcloudlookupserver-${FRONTNAME}
          - name: MYSQL_USER
            valueFrom:
              secretKeyRef:
                key: database-user
                name: nextcloudlookupserver-${FRONTNAME}
          - name: ONLY_TRUST_KUBE_CA
            value: "true"
          - name: PUBLIC_PROTO
            value: https
          - name: REPLICATION_MAIN_PASSWORD
            valueFrom:
              secretKeyRef:
                key: replication-main
                name: nextcloudlookupserver-${FRONTNAME}
          - name: REPLICATION_PROTO
            value: http
          - name: REPLICATION_SLAVE_PASSWORD
            valueFrom:
              secretKeyRef:
                key: replication-slave
                name: nextcloudlookupserver-${FRONTNAME}
          - name: TZ
            value: Europe/Paris
          imagePullPolicy: IfNotPresent
          livenessProbe:
            failureThreshold: 5
            initialDelaySeconds: 60
            periodSeconds: 60
            successThreshold: 1
            tcpSocket:
              port: 8080
            timeoutSeconds: 3
          name: nextcloudlookupserver
          ports:
          - containerPort: 8080
            protocol: TCP
          readinessProbe:
            failureThreshold: 5
            initialDelaySeconds: 60
            periodSeconds: 60
            successThreshold: 1
            tcpSocket:
              port: 8080
            timeoutSeconds: 3
          resources:
            limits:
              cpu: "${NEXTCLOUDLOOKUPSERVER_CPU_LIMIT}"
              memory: "${NEXTCLOUDLOOKUPSERVER_MEMORY_LIMIT}"
          terminationMessagePath: /dev/termination-log
          volumeMounts:
          - name: apachesites
            mountPath: /etc/apache2/sites-enabled
        volumes:
        - emptyDir: {}
          name: apachesites
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - nextcloudlookupserver
        from:
          kind: ImageStreamTag
          name: nextcloudlookupserver:${NEXTCLOUDLOOKUPSERVER_IMAGE_TAG}
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: nextcloudlookupserver-${FRONTNAME}
    annotations:
      description: Exposes NextCloud Lookup Server
  spec:
    ports:
    - name: wp
      port: 8080
      targetPort: 8080
    selector:
      name: nextcloudlookupserver-${FRONTNAME}
- apiVersion: v1
  kind: Route
  metadata:
    name: nextcloudlookupserver-${FRONTNAME}
  spec:
    host: cloud-lookup.${ROOT_DOMAIN}
    to:
      kind: Service
      name: nextcloudlookupserver-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
parameters:
- name: FRONTNAME
  description: The name identifier assigned to objects defined in this template
  displayName: Name
  required: true
  value: demo
- name: MYSQL_IMAGESTREAM_TAG
  description: MySQL ImageStream Tag
  displayName: mysql imagestream tag
  required: true
  value: mariadb:10.2
- name: MYSQL_NAMESPACE
  description: The OpenShift Namespace where the MySQL ImageStream resides
  displayName: MySQL Namespace
  required: true
  value: openshift
- name: NEXTCLOUDLOOKUPSERVER_CPU_LIMIT
  description: Maximum amount of CPU a NextCloud Lookup Server container can use
  displayName: NextCloud Lookup Server CPU Limit
  required: true
  value: 300m
- name: NEXTCLOUDLOOKUPSERVER_IMAGE_TAG
  description: The ImageStreamTag we should pull images from
  displayName: Tag
  required: true
  value: master
- name: NEXTCLOUDLOOKUPSERVER_MEMORY_LIMIT
  description: Maximum amount of memory a NextCloud Lookup Server container can use
  displayName: NextCloud Lookup Server Memory Limit
  required: true
  value: 512Mi
- name: NEXTCLOUDLOOKUPSERVER_MYSQL_CPU_LIMIT
  description: Maximum amount of CPU a NextCloud Lookup Server database container can use
  displayName: NextCloud Lookup Server MySQL CPU Limit
  required: true
  value: 500m
- name: NEXTCLOUDLOOKUPSERVER_MYSQL_MEMORY_LIMIT
  description: Maximum amount of memory a NextCloud Lookup Server database container can use
  displayName: NextCloud Lookup Server MySQL Memory Limit
  required: true
  value: 768Mi
- name: ROOT_DOMAIN
  description: Root Domain
  displayName: Root Domain
  required: true
  value: demo.local
