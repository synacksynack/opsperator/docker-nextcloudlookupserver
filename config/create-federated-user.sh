#!/bin/sh
# WARNING: requires disabling the signature verification while
# registering new users to the lookup server -- or, ideally,
# figuring out where's that private key I should sign my
# messages with. Public key can be fetched with:
# curl -k $NEXTCLOUD_GSS_MASTER/ocs/v2.php/identityproof/key/$USERNAME \
#	--header 'OCS-APIREQUEST: true' \
#	--header 'Accept: application/json' \
#	| jq .ocs.data.public

if test "$DEBUG"; then
    set -x
fi

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
NEXTCLOUD_USERNAME=${1:-admin0}
NEXTCLOUD_USERBACKEND=${2:-cloud.example.com}
NEXTCLOUD_USEREMAILDOMAIN=${3:-example.com}

curl -k -X POST -H "Content-Type: application/json" -d "{
  \"message\" : {
    \"data\" : {
      \"federationId\": \"$NEXTCLOUD_USERNAME@$NEXTCLOUD_USERBACKEND\",
      \"name\": \"$NEXTCLOUD_USERNAME\",
      \"email\": \"$NEXTCLOUD_USERNAME@$NEXTCLOUD_USEREMAILDOMAIN\",
      \"userid\": \"$NEXTCLOUD_USERNAME\"
    },
    \"type\": \"lookupserver\",
    \"timestamp\": 1337,
    \"signer\": \"$NEXTCLOUD_USERNAME@$NEXTCLOUD_USERBACKEND\"
  },
  \"signature\": \"of course\"
}" http://127.0.0.1:$APACHE_HTTP_PORT/users
