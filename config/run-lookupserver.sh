#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
LOOKUP_GLOBAL_SCALE=${LOOKUP_GLOBAL_SCALE:-true}
LOOKUP_JWT_KEY=${LOOKUP_JWT_KEY:-changeme}
MYSQL_DATABASE=${MYSQL_DATABASE:-lookup}
MYSQL_HOST=${MYSQL_HOST:-127.0.0.1}
MYSQL_PASS=${MYSQL_PASS:-secret}
MYSQL_PORT=${MYSQL_PORT:-3306}
MYSQL_USER=${MYSQL_USER:-mysql}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
REPLICATION_MAIN_PASSWORD=${REPLICATION_MAIN_PASSWORD:-secret}
REPLICATION_PROTO=${REPLICATION_PROTO:-http}
REPLICATION_SLAVE_PASSWORD=${REPLICATION_SLAVE_PASSWORD:-secret}
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=lookup.$OPENLDAP_DOMAIN
fi
export APACHE_DOMAIN
export APACHE_HTTP_PORT
export APACHE_IGNORE_OPENLDAP=yay
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

cpt=0
echo Waiting for MySQL backend ...
while true
do
    if echo SHOW TABLES | mysql -u "$MYSQL_USER" \
	    --password="$MYSQL_PASS" -h "$MYSQL_HOST" \
	    -P "$MYSQL_PORT" "$MYSQL_DATABASE" >/dev/null 2>&1; then
	echo " MySQL is alive!"
	break
    elif test "$cpt" -gt 25; then
	echo "Could not reach MySQL" >&2
	exit 1
    fi
    sleep 5
    echo MySQL is ... KO
    cpt=`expr $cpt + 1`
done

if ! echo SHOW TABLES | mysql -u "$MYSQL_USER" \
	--password="$MYSQL_PASS" -h "$MYSQL_HOST" \
	-P "$MYSQL_PORT" "$MYSQL_DATABASE" 2>/dev/null \
	| grep emailValidation >/dev/null; then
    echo Setting up database ...
    if ! cat /mysql.dmp | mysql -u "$MYSQL_USER" \
	    --password="$MYSQL_PASS" -h "$MYSQL_HOST" \
	    -P "$MYSQL_PORT" "$MYSQL_DATABASE" >/dev/null 2>&1; then
	echo "CRITICAL: failed initializing database"
    else
	echo INFO: Done initializing database
    fi
fi
if ! test -s /var/www/html/config/config.php; then
    if ! sed -e "s|APACHE_DOMAIN|$APACHE_DOMAIN|" \
	    -e "s|DBHOST|$MYSQL_HOST|" \
	    -e "s|DBNAME|$MYSQL_DATABASE|" \
	    -e "s|DBPASS|$MYSQL_PASS|" \
	    -e "s|DBPORT|$MYSQL_PORT|" \
	    -e "s|DBUSER|$MYSQL_USER|" \
	    -e "s|PUBLIC_PROTO|$PUBLIC_PROTO|" \
	    -e "s|REPLMAINPW|$REPLICATION_MAIN_PASSWORD|" \
	    -e "s|REPLSLAVEPW|$REPLICATION_SLAVE_PASSWORD|" \
	    -e "s|LOOKUPJWT|$LOOKUP_JWT_KEY|" \
	    -e "s|LOOKUPGLOBAL|$LOOKUP_GLOBAL_SCALE|" \
	    /config.php >/var/www/html/config/config.php; then
       echo "CRITICAL: failed writing configuration"
    else
	if test "$REPLICATION_TARGETS"; then
	    cat <<EOF >>/var/www/html/config/config.php
	'REPLICATION_HOSTS' => [
EOF
	    for target in $REPLICATION_TARGETS
	    do
		cat <<EOF >>/var/www/html/config/config.php
	'$REPLICATION_PROTO://lookup:$REPLICATION_SLAVE_PASSWORD@$target:$APACHE_HTTP_PORT/replication',
EOF
	    done
	    cat <<EOF >>/var/www/html/config/config.php
	]
    ];
EOF
	else
	    cat <<EOF >>/var/www/html/config/config.php
	'REPLICATION_HOSTS' => []
    ];
EOF
	fi
	echo INFO: Done initializing NextCloud Lookup Server Configuration
    fi
fi

if ! ls /etc/apache2/sites-enabled/*.conf >/dev/null 2>&1; then
    echo Generates NextCloud Lookup Server VirtualHost Configuration
    sed -e "s|LOOKUP_HOSTNAME|$APACHE_DOMAIN|g" \
	-e "s|HTTP_PORT|$APACHE_HTTP_PORT|g" \
	-e "s|SSL_INCLUDE|$SSL_INCLUDE.conf|g" \
	/vhost.conf >/etc/apache2/sites-enabled/003-vhosts.conf
fi

unset MYSQL_USER MYSQL_HOST MYSQL_DATABASE MYSQL_PORT MYSQL_PASS \
    LOOKUP_JWT_KEY REPLICATION_MAIN_PASSWORD REPLICATION_SLAVE_PASSWORD \
    LOOKUP_VERSION

. /run-apache.sh
