<?php

$CONFIG = [
	'DB' => [
		'host' => 'DBHOST',
		'db' => 'DBNAME',
		'user' => 'DBUSER',
		'pass' => 'DBPASS',
	    ],
	'ERROR_VERBOSE' => true,
	'LOG' => '/proc/self/fd/1',
	'REPLICATION_LOG' => '/proc/self/fd/1',
	'MAX_SEARCH_PAGE' => 25,
	'MAX_REQUESTS' => 10000,
	'REPLICATION_AUTH' => 'REPLMAINPW',
	'SLAVEREPLICATION_AUTH' => 'REPLSLAVEPW',
	'IP_BLACKLIST' => [],
	'SPAM_BLACKLIST' => [],
	'EMAIL_SENDER' => 'admin@APACHE_DOMAIN',
	'PUBLIC_URL' => 'PUBLIC_PROTO://APACHE_DOMAIN/nextcloud/lookup-server',
	'GLOBAL_SCALE' => LOOKUPGLOBAL,
	'AUTH_KEY' => 'LOOKUPJWT',
	'TWITTER' => [
		'CONSUMER_KEY' => '',
		'CONSUMER_SECRET' => '',
		'ACCESS_TOKEN' => '',
		'ACCESS_TOKEN_SECRET' => '',
	    ],
