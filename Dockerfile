FROM docker.io/composer:1.7 as bootstrap

ENV LOOKUP_REPO=https://github.com/Worteks/lookup-server \
    LOOKUP_VERSION=noverify-update

RUN git clone --branch $LOOKUP_VERSION $LOOKUP_REPO \
    && composer install --ignore-platform-reqs --no-interaction --no-plugins \
	--no-scripts --prefer-dist --optimize-autoloader \
	--working-dir /app/lookup-server/server

FROM opsperator/php

ARG DO_UPGRADE=
ENV LANG=en_US.UTF-8 \
    LOOKUP_VERSION=v0.3.2

# NextCloud Lookup Server image for OpenShift Origin

LABEL io.k8s.description="NextCloud Lookup Server" \
      io.k8s.display-name="NextCloud Lookup Server $LOOKUP_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="syspass,passwords" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-nextcloudlookupserver" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$LOOKUP_VERSION"

USER root

COPY config/* /
COPY --from=bootstrap /app/lookup-server/server /var/www/html/
COPY --from=bootstrap /app/lookup-server/mysql.dmp /
COPY --from=bootstrap /usr/bin/composer /usr/bin/

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && sed -i \
	-e '/^CREATE DATABASE/d' \
	-e '/^USE /d' /mysql.dmp \
    && mv /.htaccess /var/www/html/ \
    && echo "# Install NextCloud Lookup Server dependencies" \
    && apt-get -y --no-install-recommends install locales locales-all \
	mariadb-client  \
    && echo en_US.UTF-8 UTF-8 >/etc/locale.gen \
    && echo LANG=en_US.UTF-8 >/etc/default/locale \
    && dpkg-reconfigure locales \
    && update-locale LANG=en_US.UTF-8 \
    && export LANG=en_US.UTF-8 \
    && sed -i "s|LANG=.*|LANG=en_US.UTF-8|g" /etc/apache2/envvars \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get -y --no-install-recommends install libcurl4-openssl-dev \
	libfreetype6-dev libicu-dev libldb-dev libonig-dev \
	libreadline-dev libjpeg62-turbo-dev \
    && docker-php-ext-install mbstring intl opcache pdo_mysql gettext \
    && docker-php-ext-enable gettext intl pdo_mysql \
    && echo "# Enabling NextCloud Lookup Server Modules" \
    && a2enmod ssl rewrite \
    && echo "# Fixing permissions" \
    && for dir in /var/www/html/config /usr/local/etc/php/conf.d \
	    /var/www/html; \
	do \
	    mkdir -p "$dir" 2>/dev/null \
	    && chown -R 1001:root "$dir" \
	    && chmod -R g=u "$dir"; \
	done \
    && echo "# Cleaning up" \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/usr/src/php.tar.xz /etc/apache2/sites-enabled/* \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-lookupserver.sh"]
USER 1001
